//
//  CalculatorViewController.swift
//  Tipsy
//
//  Created by Angela Yu on 09/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {

    @IBOutlet weak var billTextField: UITextField!
    @IBOutlet weak var zeroPctButton: UIButton!
    @IBOutlet weak var tenPctButton: UIButton!
    @IBOutlet weak var twentyPctButton: UIButton!
    @IBOutlet weak var splitNumberLabel: UILabel!
    
    var tipBrain = TipBrain()
    
    
    @IBAction func tipChanged(_ sender: UIButton) {
        
        zeroPctButton.isSelected = false
        tenPctButton.isSelected = false
        twentyPctButton.isSelected = false
        billTextField.endEditing(true)
        
        if sender.titleLabel?.text == "0%" {
            zeroPctButton.isSelected = true
        } else if sender.titleLabel?.text == "10%" {
            tenPctButton.isSelected = true
        } else {
            twentyPctButton.isSelected = true
        }
        
        //Drop last will remove the final character from a string.
        //Also need to cast as a String Initially
        let buttonTitleMinusPercentSign = String((sender.currentTitle?.dropLast())!)
        
        //Cast the String to a Double and divide by 100 to get the decimal
        tipBrain.tipPercent = (Double(buttonTitleMinusPercentSign)! / 100)
    }
    
    
    @IBAction func stepValueChanged(_ sender: UIStepper) {
        tipBrain.stepperValue = Int(sender.value)
        splitNumberLabel.text = String(tipBrain.stepperValue)
    }
    
    @IBAction func calculatePressed(_ sender: UIButton) {
        let bill = Double(billTextField.text!) ?? 0.0
        let totalTip = bill * tipBrain.tipPercent
        
        tipBrain.total = tipBrain.calculateTip(bill: bill, totalTip: totalTip)
        
        performSegue(withIdentifier: "goToResult", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult" {
            let destinationVC = segue.destination as! ResultsViewController
            destinationVC.tipTotal = tipBrain.getTipTotal()
            destinationVC.tipSettings = tipBrain.getTipSettings()
        }
    }
}

