//
//  ResultsViewController.swift
//  Tipsy
//
//  Created by Taylor Potts on 2020-07-09.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {
    var tipTotal: String = ""
    var tipSettings: String = ""
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    
    @IBAction func recalculatePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totalLabel.text = tipTotal
        settingsLabel.text = tipSettings
    }
}
