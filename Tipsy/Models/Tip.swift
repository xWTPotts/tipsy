//
//  Tip.swift
//  Tipsy
//
//  Created by Taylor Potts on 2020-07-09.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct Tip {
    var tipTotal: String
    var settings: String
}
