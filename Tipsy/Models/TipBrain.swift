//
//  TipBrain.swift
//  Tipsy
//
//  Created by Taylor Potts on 2020-07-09.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation


struct TipBrain {
    var tipPercent = 0.0
    var stepperValue = 1
    var total = ""
    
    func getTipTotal() -> String {
        return total
    }
    
    func getTipSettings() -> String {
        let tipPercent = Int(self.tipPercent * 100)
        let settingsTitle =  "Split between \(stepperValue) people with \(tipPercent)% tip."
        
        return settingsTitle
    }
    
    
    mutating func calculateTip(bill: Double, totalTip: Double) -> String {
        
        // For individuals to pay an even tip
        // total = String(format: "%.2f", totalTip / Double(stepperValue))
        total = String(format: "%.2f", (totalTip + bill) / Double(stepperValue))
        return total
        
    }
}
